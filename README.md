# EmuMap
Emulator-agnostic keybindings for X11 based Linux systems

---

## Requirements
- Python 3 (tested with 3.8.2)
- X11 (won't work with Wayland, and probably never will)
- ADB (used to send touch events)
- Android based emulator

## Usage

Install required Python libraries with pipenv or a package manager of your choice.

```sh
pipenv install
```

Then simply run the main script.

```sh
./emumap.sh
```

## Definitions
Each defninition consists of multiple scenes, which in turn defines the labelled screen regions associated with that scene.

Those scenes and labels can be referenced in the key map configuration.
