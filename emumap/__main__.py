from queue import Queue
from threading import Thread

from .keyreader import KeyReader
from .focus_observer import WindowFocusObserver
from .logger import LOGGER as LOG
from .config import load_app_config
from .device import Device
from .definition import find_definition


config = load_app_config()
dev = Device(config.device_serial)

definition = find_definition(config.keymap.app_id, dev.screen_width, dev.screen_height, config.only_scalable)

if not definition:
    LOG.error("Could not find matching definition for %s", config.keymap.app_id)
    exit(1)

LOG.info("Loaded definition for %s", config.keymap.app_id)

queue = Queue()

krd = KeyReader()
krd.set_queue(queue)

obsv = WindowFocusObserver()
obsv.on_focus_enter(lambda: krd.enable())
obsv.on_focus_leave(lambda: krd.disable())
obsv.observe(wm_class=config.observed_wm_class)

LOG.info("Observing focus change for %s", config.observed_wm_class)

scene_mapping = config.keymap.get_default()
scene = definition.scenes[scene_mapping.scene_id]
screen = dev.screen_rect
screen_areas = {rid: r.get_rect(screen) for rid, r in scene.regions.items()}


def handle_key(keysym):
    region_id = scene_mapping[keysym]
    if not region_id: return

    LOG.debug("Got keypress for %s/%s", scene.name, region_id)

    rect = screen_areas[region_id]
    if not rect: return

    dev.touch_inside_region(rect)


while True:
    keysym = queue.get()
    Thread(target=handle_key, args=(keysym,)).start()
