from random import randint
from .adb import ADB
from .logger import LOGGER as LOG
from .geometry import ScreenRect


class Device(object):
    def __init__(self, serial):
        self._adb = ADB(serial)
        self._display_size = self._adb.get_display_size()

    @property
    def screen_width(self):
        return self._display_size[0]

    @property
    def screen_height(self):
        return self._display_size[1]

    @property
    def screen_rect(self):
        return ScreenRect(0, 0, self.screen_width, self.screen_height)

    def touch_inside_region(self, rect: ScreenRect):
        x, y = rect.random_point()
        LOG.debug("Sending touch event to (%d, %d)", x, y)
        self._adb.send_touch_event(x, y)
