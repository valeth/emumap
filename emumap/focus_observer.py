from threading import Thread

from Xlib import X
from Xlib.error import BadWindow
from Xlib.display import Display

from .logger import LOGGER as LOG


class WindowFocusObserver(object):
    def __init__(self):
        self._observed_wm_class = None
        self._display = Display()
        self._root_win = self._display.screen().root
        self._root_win.change_attributes(event_mask = X.PropertyChangeMask)

        self._net_active_window = self._display.intern_atom("_NET_ACTIVE_WINDOW")

        self._focused = False
        self._thread = None
        self._on_enter_callback = None
        self._on_leave_callback = None


    def observe(self, *, wm_class):
        if self._thread: return

        self._observed_wm_class = wm_class
        self._thread = Thread(target=self._focus_handler)
        self._thread.daemon = True
        self._thread.start()

    def on_focus_enter(self, f):
        self._on_enter_callback = f

    def on_focus_leave(self, f):
        self._on_leave_callback = f


    def _on_enter(self):
        if callable(self._on_enter_callback):
            self._on_enter_callback()

    def _on_leave(self):
        if callable(self._on_leave_callback):
            self._on_leave_callback()

    def _get_active_window_id(self):
        return self._root_win.get_full_property(self._net_active_window, X.AnyPropertyType).value[0]

    def _get_active_window(self):
        wid = self._get_active_window_id()
        return self._display.create_resource_object("window", wid)

    def _get_active_window_class(self):
        try:
            win = self._get_active_window()
            w_class, _ = win.get_wm_class()
        except BadWindow:
            w_class = None

        return w_class

    def _handle_event(self, event):
        if event.type != X.PropertyNotify: return
        if event.atom != self._net_active_window: return

        active_window_class = self._get_active_window_class()

        if not active_window_class:
            LOG.warn("Window had no valid WM_CLASS property")
            return

        if self._focused:
            if active_window_class != self._observed_wm_class:
                LOG.debug("Lost focus")
                self._focused = False
                self._on_leave()
        else:
            if active_window_class == self._observed_wm_class:
                LOG.debug(f"Got focus to {active_window_class}")
                self._focused = True
                self._on_enter()

    def _focus_handler(self):
        while True:
            event = self._display.next_event()
            self._handle_event(event)
