from random import randint
import re


class InvalidValue(Exception):
    pass


class Length(object):
    def __init__(self, value: [int, float], rel=False):
        if isinstance(value, (int, float)):
            self.value = float(value)
            self.relative = rel
        elif isinstance(value, str):
            self.value, self.relative = self._parse_value(value)
        else:
            raise TypeError

    def __add__(self, other):
        return self.value + other

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        return self.value + -other

    def __rsub__(self, other):
        return self - other

    def __neg__(self):
        return -self.value

    def __truediv__(self, other):
        return self.value / other

    def __mul__(self, other):
        return self.value * other

    def __rmul__(self, other):
        return self * other

    def __int__(self):
        return int(self.value)

    def __str__(self):
        if self.relative:
            return f"{self.value * 100}%"
        else:
            return str(self.value)

    def __repr__(self):
        return str(self)

    @staticmethod
    def _parse_value(value: str) -> (int, bool):
        match = re.search(r"^(-?\d+\.?\d*?)(px|%)?$", value)
        if not match:
            raise InvalidValue(f"Value {value} is not valid length value")

        val, kind = match.groups()
        if not val:
            raise InvalidValue("Length has to be a number")

        rel = kind == "%"
        val = float(val) / 100 if rel else float(val)

        return val, rel


class ScreenRect(object):
    def __init__(self, x: int, y: int, w: int, h: int):
        self.x_pos = x
        self.y_pos = y
        self.width = w
        self.height = h

    def random_point(self) -> (int, int):
        """Returns a random point inside of the region."""

        x = randint(self.x_pos, self.x_pos + self.width)
        y = randint(self.y_pos, self.y_pos + self.height)
        return x, y
