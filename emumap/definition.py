from pathlib import Path
from enum import Enum

import yaml

from .logger import LOGGER as LOG
from .geometry import Length, ScreenRect


class InvalidDefinition(Exception):
    pass


class Anchor(Enum):
    top = 1
    bottom = 2
    left = 4
    right = 8
    center = 16


class Region(object):
    def __init__(self, rid: int, anchors: (Anchor, Anchor), pos: (Length, Length), dim: (Length, Length), scalers: (str, str)):
        self.id = rid
        self.x_anchor, self.y_anchor = anchors
        self.x_pos, self.y_pos = pos
        self.width, self.height = dim
        self.x_scaler, self.y_scaler = scalers

    def get_rect(self, screen: ScreenRect) -> ScreenRect:
        x_scaler = screen.width if self.x_scaler == "width" else screen.height
        y_scaler = screen.height if self.y_scaler == "height" else screen.width
        x_pos, width = self._calculate_rect(self.x_pos, self.width, self.x_anchor, x_scaler)
        y_pos, height = self._calculate_rect(self.y_pos, self.height, self.y_anchor, y_scaler)
        return ScreenRect(x_pos, y_pos, width, height)

    @staticmethod
    def _calculate_rect(a: Length, b: Length, anchor: Anchor, screen_dim: int) -> (int, int):
        start = int(screen_dim * a) if a.relative else int(a)
        end = int(screen_dim * b) if b.relative else int(b)

        if anchor in [Anchor.right, Anchor.bottom]:
            return start - end, end
        elif anchor == Anchor.center:
            return start - int(end / 2), end
        else:
            return start, end

    @classmethod
    def from_dict(cls, spec):
        try:
            x_anchor = Anchor[spec.get("x_anchor", "left")]
            y_anchor = Anchor[spec.get("y_anchor", "top")]
            x_pos = Length(spec["x_pos"])
            y_pos = Length(spec["y_pos"])
            y_scaler = spec.get("y_scaler", "height")
            x_scaler = spec.get("x_scaler", "width")
            width = Length(spec["width"])
            height = Length(spec["height"])
            return cls(spec["id"], (x_anchor, y_anchor), (x_pos, y_pos), (width, height), (x_scaler, y_scaler))
        except KeyError as e:
            raise InvalidDefinition(e)


class Scene(object):
    def __init__(self, name, regions = []):
        self.name = name
        self.regions = {r.id: r for r in regions}

    def add_region(self, region):
        self.regions[region.id] = region

    def __getitem__(self, region_id):
        if region_id in self.regions:
            return self.regions[region_id]
        else:
            return None


class Definition(object):
    def __init__(self, app_id, version, scalable, device_width, device_height, scenes = []):
        self.app_id = app_id
        self.version = version
        self.scalable = scalable
        self.device_width = device_width
        self.device_height = device_height
        self.scenes = { s.name: s for s in scenes }

    def add_scene(self, scene):
        self.scenes[scene.name] = scene

    @classmethod
    def from_dict(cls, spec):
        try:
            width = spec.get("device_width")
            height = spec.get("device_height")
            scalable = spec.get("scalable") or not (height or width)

            if not scalable and not (height and width):
                raise InvalidDefinition("Needs to have either height and width, or be set to scalable")

            return cls(spec["app_id"], spec["version"], scalable, width, height, spec["scenes"])
        except KeyError:
            raise InvalidDefinition


def load_regions(specs) -> [Region]:
    return [Region.from_dict(spec) for spec in specs]


def load_scene(path: Path) -> Scene:
    spec = yaml.safe_load(path.read_text())
    regions = load_regions(spec["regions"])
    return Scene(spec["name"], regions)


def load_scenes(path: Path) -> [Scene]:
    return [load_scene(f) for f in path.iterdir()]


def load_definition(path: Path) -> Definition:
    manifest_path = path / "manifest.yml"
    spec = yaml.safe_load(manifest_path.open())
    spec["scenes"] = load_scenes(path / "scenes")
    return Definition.from_dict(spec)


def load_definitions(app_id: str) -> [Definition]:
    return [load_definition(p.parent) for p in Path(f"./definitions/{app_id}").glob("**/manifest.yml")]


def find_definition(app_id: str, width: int, height: int, only_scalable=False) -> Definition or None:
    fallback = None
    definition = None

    for d in load_definitions(app_id):
        if d.app_id != app_id:
            continue

        if d.scalable:
            if only_scalable:
                definition = d
                break
            else:
                fallback = d
                continue

        if only_scalable:
            continue

        if d.device_width == width and d.device_height == height:
            definition = d
            break

    definition = definition or fallback

    if definition:
        LOG.info("Found definition for %s: (width=%s, height=%s, scalable=%s)",
                 definition.app_id, definition.device_width, definition.device_height, definition.scalable)

    return definition
