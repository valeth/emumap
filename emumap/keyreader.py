from threading import Thread
from queue import Full as QueueFull

from Xlib import X, XK
from Xlib.display import Display
from Xlib.ext import record
from Xlib.protocol import rq


class KeyReader(object):
    def __init__(self):
        self._display = Display()
        self._recording_ctx = self._display.record_create_context(
            0, [record.AllClients], [{
                "core_requests": (0, 0),
                "core_replies": (0, 0),
                "ext_requests": (0, 0, 0, 0),
                "ext_replies": (0, 0, 0, 0),
                "delivered_events": (0, 0),
                "device_events": (X.KeyPress, X.KeyPress),
                "errors": (0, 0),
                "client_started": False,
                "client_died": False,
            }]
        )
        self._enabled = False
        self._thread = None
        self._queue = None

    def __del__(self):
       self._display.record_free_context(self._recording_ctx)


    @property
    def active(self):
        return self._thread != None

    def set_queue(self, q):
        self._queue = q

    def enable(self):
        if self.active: return

        self._enabled = True
        self._thread = Thread(target=self._enable_context, name="KeyReader")
        self._thread.daemon = True
        self._thread.start()

    def disable(self):
        if not self.active: return

        self._enabled = False
        self._thread.join()
        self._thread = None


    def _parse_reply(self, data):
        dsp = self._display.display
        return rq.EventField(None).parse_binary_value(data, dsp, None, None)

    def _try_put_into_queue(self, item):
        if not self._queue: return

        try:
            self._queue.put(item, False, timeout=0.025)
        except QueueFull:
            pass

    def _enable_context(self):
        self._display.record_enable_context(self._recording_ctx, self._record_handler)

    def _disable_context(self):
        ldisplay = Display()
        ldisplay.record_disable_context(self._recording_ctx)
        ldisplay.flush()

    def _handle_key_press(self, event):
        keysym = self._display.keycode_to_keysym(event.detail, 0)
        self._try_put_into_queue(keysym)

    def _handle_key_release(self, event):
        pass

    def _record_handler(self, reply):
        if not self._enabled:
            self._disable_context()
            return

        if reply.category != record.FromServer or reply.client_swapped:
            return

        data = reply.data

        while (data):
            event, data = self._parse_reply(data)

            if event.type == X.KeyPress:
                self._handle_key_press(event)
            elif event.type == X.KeyRelease:
                self._handle_key_release(event)
