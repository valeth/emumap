import subprocess

from .logger import LOGGER as LOG


class ADBError(Exception):
    pass

class ExecutionFailed(ADBError):
    pass


class ADB(object):
    def __init__(self, device_serial):
        self._device_serial = device_serial

    def send_touch_event(self, x, y):
        self._execute("input", "tap", x, y)

    def enable_pointer_overlay(self):
        self._put_setting("system", "pointer_location", "1")

    def disable_pointer_overlay(self):
        self._put_setting("system", "pointer_location", "0")

    def get_display_size(self):
        raw = self._execute("wm", "size")
        return tuple(map(int, raw.split(": ")[-1].split("x")))

    def _put_setting(self, namespace, key, value):
        self._execute("settings", "put", namespace, key, value)

    def _devices(self):
        return self._adb("devices", "-l")

    def _execute(self, *args):
        return self._adb("exec-out", *args)

    def _execute_shell(self, *args):
        return self._adb("shell", *args)

    def _adb(self, *args):
        try:
            string_args = list(map(str, args))
            ret = subprocess.check_output(["adb", "-s", self._device_serial, *string_args])
            return ret.decode().strip()
        except subprocess.CalledProcessError as e:
            LOG.error(e)
            # raise ExecutionFailed(e)
