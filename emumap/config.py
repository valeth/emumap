from pathlib import Path
from itertools import chain

import yaml


class ApplicationConfig(object):
    def __init__(self, wm_class, device_serial, only_scalable):
        self.observed_wm_class = wm_class
        self.device_serial = device_serial
        self.only_scalable = only_scalable
        self.keymap = None

    def set_active_keymap(self, keymap):
        self.keymap = keymap


class KeyMap(object):
    def __init__(self, app_id, name, default_scene):
        self.name = name
        self.app_id = app_id
        self.default_scene = default_scene
        self._scene_mappings = {}

    def add_scene_mapping(self, mapping):
        self._scene_mappings[mapping.scene_id] = mapping

    def for_scene(self, scene_id):
        try:
            return self._scene_mappings[scene_id]
        except KeyError:
            return []

    def get_default(self):
        return self.for_scene(self.default_scene)

    @property
    def region_ids(self):
        return set(chain(*[s.region_ids for s in self._scene_mappings.values()]))

    @property
    def scene_ids(self):
        return set(self._scene_mappings.keys())


class SceneMapping(object):
    def __init__(self, scene):
        self.scene_id = scene
        self._mappings = {}

    def __getitem__(self, key):
        k = chr(key) if isinstance(key, int) else key
        if k in self._mappings:
            return self._mappings[k]
        else:
            return None

    def __setitem__(self, key, region_id):
        self._mappings[key] = region_id

    @property
    def region_ids(self):
        return set(self._mappings.values())


def load_keymap(path: Path) -> KeyMap:
    keymap_name = path.stem

    with open(path) as f:
        config = yaml.safe_load(f.read())

    keymap = KeyMap(config["app_id"], keymap_name, config["default_scene"])

    for scene, mappings in config["keymaps"].items():
        scene_mapping = SceneMapping(scene)

        for m in mappings:
            scene_mapping[m["key"]] = m["region"]

        keymap.add_scene_mapping(scene_mapping)

    return keymap


def load_app_config() -> ApplicationConfig:
    path = Path("./config/emumap.yml")

    with open(path) as f:
        config = yaml.safe_load(f.read())

    only_scalable = config.get("only_scalable", False)
    app_config = ApplicationConfig(config["observed_wm_class"], config["device_serial"], only_scalable)

    keymap_name = config["active_keymap"]
    keymap_path = path.parent / "keymaps" / f"{keymap_name}.yml"
    keymap = load_keymap(keymap_path)

    app_config.set_active_keymap(keymap)

    return app_config
