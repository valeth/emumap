import logging

LOG_FORMAT = "[%(levelname)s] %(asctime)s: %(message)s"
logging.basicConfig(format=LOG_FORMAT, level=logging.DEBUG)

LOGGER = logging.getLogger("EmuMap")
